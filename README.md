# dockerodoo
odoo docker support init batch

## docker用户权限和宿主机权限对应关系

不打开用户命名空间remap功能情况下，docker容器内用户的ID就对应宿主机的用户ID，比如：

宿主机：
- 用户名：ubuntu
- 用户ID：1000

容器内:
- 用户名：odoo
- 用户ID：1000

在容器内使用odoo用户创建的文件都对应宿主机ubuntu用户创建文件。

开用户命名空间remap功能情况下：

```sh
$ cat /etc/subuid
ubuntu:100000:65536

$ cat /etc/docker/daemon.json
{
  "userns-remap": "ubuntu"
}
```
docker设置使用了ubuntu的命名空间映射，那么docker容器内用户创建的文件，就会使用宿主内用户id(1000)加上映射值(100000)作为实际创建外部文件的用户id。

利用这一点，我们可以通过设置subuid里的ubuntu为1000:65536,就可以把容器内部root用户映射到宿主机的用户ubuntu(1000)上。

**注意：**

设定完成后需要重新启动，否则会出现docker创建的网络无法访问。

## TODO

- [x] 用户名空间映射
    - [x] 弄清用户名空间映射机制，文档记录
    - [x] 完成用户名空间映射设定
- [x] 配置vscode进行docker容器开发
- [x] 创建自动安装新模块