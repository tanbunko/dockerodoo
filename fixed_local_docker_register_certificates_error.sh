BUILDER=$(sudo docker ps | grep buildkitd | cut -f1 -d' ')
sudo docker cp "/etc/docker/certs.d/192.168.10.100:5000/registry.crt" $BUILDER:/usr/local/share/ca-certificates/
sudo docker exec $BUILDER update-ca-certificates
sudo docker restart $BUILDER
